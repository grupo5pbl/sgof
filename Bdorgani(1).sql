-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 01, 2015 at 07:50 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Bdorgani`
--

-- --------------------------------------------------------

--
-- Table structure for table `Curso`
--

CREATE TABLE IF NOT EXISTS `Curso` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `alvo` varchar(255) NOT NULL,
  `dataInicio` date NOT NULL,
  `dataFim` date NOT NULL,
  `publicado` int(11) NOT NULL,
  `imagem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `Curso`
--

INSERT INTO `Curso` (`id`, `nome`, `descricao`, `area`, `alvo`, `dataInicio`, `dataFim`, `publicado`, `imagem_id`) VALUES
(8, 'asas', '	\r\n					asasa', 'informatica', 'docente', '2015-05-13', '2015-09-13', 1, 0),
(9, 'Tf', 'Grupo de adolescentes', 'informatica', 'docente', '2015-05-13', '2015-09-13', 1, 0),
(10, 'Tf', 'Grupo de adolescentes', 'informatica', 'docente', '2015-05-13', '2015-09-13', 1, 0),
(11, 'sdadadad', '	as\r\n							', 'informatica', 'docente', '2015-02-02', '2015-02-02', 1, 0),
(12, 'dsc', '	\r\n							sds', 'informatica', 'docente', '2015-05-13', '2015-05-13', 1, 0),
(13, 'dsc', '	\r\n							sds', 'informatica', 'docente', '2015-05-13', '2015-05-13', 1, 0),
(14, 'as', '	\r\n							asas', 'informatica', 'docente', '2015-05-13', '2015-09-13', 1, 0),
(15, 'as', '	\r\n							asas', 'informatica', 'docente', '2015-05-13', '2015-09-13', 1, 0),
(16, 'asssssssssssssserr', '	\r\n							asas', 'informatica', 'docente', '2015-05-13', '2015-09-13', 1, 0),
(17, 'sdfsf', '	sdsfsdf\r\n							', 'informatica', 'docente', '2015-05-13', '2015-05-13', 1, 0),
(18, 'sdfsf', '	sdsfsdf\r\n							', 'informatica', 'docente', '2015-05-13', '2015-05-13', 1, 0),
(19, 'm,m', '	m,.m,m,.\r\n							', 'informatica', 'docente', '2015-05-13', '2015-02-02', 1, 0),
(20, 'klnlkk', '	\r\n							,mm,m', 'informatica', 'docente', '2015-02-02', '2015-02-02', 1, 0),
(21, 'asa', '	\r\n					xzxzx', 'informatica', 'docente', '2015-05-13', '2015-09-13', 1, 0),
(23, 'dsds', '	\r\n							ghghghgh', 'informatica', 'docente', '2015-05-13', '2015-05-13', 1, 0),
(27, 'dsdsmnm', '	\r\n							ghghghgh./', 'informatica', 'docente', '2015-05-13', '2015-05-13', 1, 0),
(28, 'dsdsmnm', '	\r\n							ghghghgh./', 'informatica', 'docente', '2015-05-13', '2015-05-13', 1, 0),
(29, 'dsdsmnm', '	\r\n							ghghghgh./', 'informatica', 'docente', '2015-05-13', '2015-05-13', 1, 0),
(30, 'dsdsmnm', '	\r\n							ghghghgh./', 'informatica', 'docente', '2015-05-13', '2015-05-13', 1, 0),
(31, 'dsdsmnmlkjlkjlj', '	\r\n		kjkjkl					ghghghgh./', 'informatica', 'docente', '2015-05-13', '2015-05-13', 1, 0),
(32, 'sas', '	\r\n							sas', 'informatica', 'docente', '2015-05-13', '2015-09-13', 1, 0),
(33, 'sas', '	\r\n							sas', 'informatica', 'docente', '2015-05-13', '2015-09-13', 1, 0),
(34, '', '', '', '', '0000-00-00', '0000-00-00', 0, 0),
(35, 'dsdf', '	\r\n			sddsd				', 'informatica', 'docente', '2015-05-13', '2015-09-13', 1, 0),
(36, 'Tf teen', 'Grupo de adolescentes	\r\n							', 'outra', 'funcionario', '2015-05-13', '2015-09-13', 1, 0),
(37, 'Tf teen', 'Grupo de adolescentes	\r\n							', 'outra', 'funcionario', '2015-05-13', '2015-09-13', 1, 0),
(38, 'domingos', 'Grupo de adolescentes	\r\n							', 'informatica', 'funcionario', '2015-05-13', '2015-09-13', 1, 0),
(39, 'A palavra', '	\r\n							jashdjsa', 'informatica', 'docente', '2015-02-02', '2015-02-02', 1, 0),
(40, 'A palavra', '	\r\n							jashdjsa', 'informatica', 'docente', '2015-02-02', '2015-02-02', 1, 0),
(41, 'A palavra', '	\r\n							jashdjsa', 'electronica', 'funcionario', '2015-02-02', '2015-09-13', 1, 0),
(42, 'A palavra', '	\r\n							jashdjsa', 'electronica', 'funcionario', '2015-02-02', '2015-09-13', 1, 0),
(43, '', '	\r\n							', '', '', '2015-05-13', '2015-09-13', 1, 0),
(44, 'Culinaria Domingos', '	\r\n							', '', '', '2015-05-13', '2015-09-13', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Imagem`
--

CREATE TABLE IF NOT EXISTS `Imagem` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` mediumblob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `Organizacao`
--

CREATE TABLE IF NOT EXISTS `Organizacao` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nomeOrg` varchar(255) NOT NULL,
  `emailOrg` varchar(255) NOT NULL,
  `tellOrg` int(255) NOT NULL,
  `localizacao` varchar(255) NOT NULL,
  `idResponsavel` int(255) NOT NULL,
  `criado` date NOT NULL,
  `actualizado` date NOT NULL,
  `id_imagem` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `Responsavel`
--

CREATE TABLE IF NOT EXISTS `Responsavel` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `apelido` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `tell` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `acesso` int(255) NOT NULL,
  `idImagem` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;